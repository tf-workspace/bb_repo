terraform {
 backend "remote" {
		
	hostname = "app.terraform.io"
    organization = "tf_nbs_org"
    workspaces {
      name = "tf_wp"
    }
 }
}