terraform {
	required_providers {
    aws = {
		source  = "hashicorp/aws"
		version = "~> 3.27"
    }
  }
}

provider "aws" {
	region = "us-east-1"
}

resource "aws_instance" "MyEc2Instance" {
  ami           = "ami-02642c139a9dfb378"
  instance_type = "t2.micro"
  tags = {
    Name = "AsifEC2Ins"
  }
}